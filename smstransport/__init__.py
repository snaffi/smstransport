from django.conf import settings
from django.utils import importlib
from django.core.exceptions import ImproperlyConfigured

if not hasattr(settings, 'SMS_TRANSPORTS'):
    raise ImproperlyConfigured('The SMS_TRANSPORTS setting is required.')

DEFAULT_TRANSPORT_ALIAS = 'default'


def import_class(path):
    path_parts = path.split('.')
    class_name = path_parts.pop()
    module_path = '.'.join(path_parts)
    module_itself = importlib.import_module(module_path)

    if not hasattr(module_itself, class_name):
        raise ImportError(
            "The Python module '%s' " +
            "has no '%s' class." % (module_path, class_name))

    return getattr(module_itself, class_name)


def load_backend(full_backend_path):
    path_parts = full_backend_path.split('.')

    if len(path_parts) < 2:
        raise ImproperlyConfigured(
            "The provided backend '%s'" +
            " is not a complete Python path" +
            " to a BaseSmsTrasportBackend subclass." % full_backend_path)

    return import_class(full_backend_path)


class TransportHandler(object):

    def __init__(self, transports_info):
        self.transports_info = transports_info
        self._transports = {}

    def __getitem__(self, key):
        if key in self._transports:
            return self._transports[key]

        params = {}
        if 'PARAMS' in self.transports_info[key]:
            params = self.transports_info[key]['PARAMS']
        self._transports[key] = load_backend(
            self.transports_info[key]['BACKEND'])(**params)
        return self._transports[key]


smstransports = TransportHandler(settings.SMS_TRANSPORTS)


class DefaultTransportProxy(object):

    def __getattr__(self, name):
        return getattr(smstransports[DEFAULT_TRANSPORT_ALIAS], name)

    def __setattr__(self, name, value):
        return setattr(smstransports[DEFAULT_TRANSPORT_ALIAS], name, value)

    def __delattr__(self, name):
        return delattr(smstransports[DEFAULT_TRANSPORT_ALIAS], name)

    def __contains__(self, key):
        return key in smstransports[DEFAULT_TRANSPORT_ALIAS]

    def __eq__(self, other):
        return smstransports[DEFAULT_TRANSPORT_ALIAS] == other

    def __ne__(self, other):
        return smstransports[DEFAULT_TRANSPORT_ALIAS] != other

smstransport = DefaultTransportProxy()
