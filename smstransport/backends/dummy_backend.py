

class BaseSmsTrasportBackend(object):

    def __init__(self, **kwargs):
        self.params = kwargs

    def send(self, phone, text):
        raise NotImplementedError()

    def check(self, sms_id):
    	raise NotImplementedError()


class DummySmsTransportBackend(BaseSmsTrasportBackend):

    def send(self, phone, text):
        print phone, text

    def check(self, sms_id):
    	print sms_id