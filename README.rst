============
Sms transport
============
Sms transport простое приложение предоставляющее интерфейс для отправки смс сообщений

Установка
============
1. Установите приложение в своё виртуальное окружение

.. code-block:: python

	pip install -e git+git@bitbucket.org:snaffi/smstransport.git#egg=smstransport


2. Добавьте "smstransport" в INSTALLED_APPS в настройках вашего Django проекта

.. code-block:: python

	INSTALLED_APPS = (
	        ...
	        'smstransport',
	    )

3. Настройка смс транспортов осуществляется в следующем формате

.. code-block:: python

	SMS_TRANSPORTS = {
	    'default': {
	        'BACKEND': 'smsgate.backends.dummy_backend.DummySmsTransportBackend',
	        'PARAMS': {
	            'login': 'some_login',
	            'password': 'some_password',
	        }
	    },
	    'paramless': {
	        'BACKEND': 'smsgate.backends.dummy_backend.DummySmsTransportBackend',        
	    }
	    'other': {
	        'BACKEND': 'smsgate.backends.dummy_backend.DummySmsTransportBackend',
	        'PARAMS': {
	            'login': 'some_login',
	            'password': 'some_password',
	            'var1': 'var1',
	            'var2': 'var2',
	        }
	    }
	}


4. Использование 

.. code-block:: python

        from smstransport import smstransport, smstransports
        
        print("Send default transport")
        smstransport.send("+79999999999", "text")

        print("Send custom transport")
        smstransports['custom'].send("+79999999999", "text")



5. Для реализации собственного бэкэнда необходимо 
   реализовать интерфейс

.. code-block:: python

        smsgate.backends.dummy_backend.BaseSmsTrasportBackend.